mangoes\.modeling package
======================

Submodules
----------

.. toctree::

   mangoes.modeling.transformer_base
   mangoes.modeling.finetuning
   mangoes.modeling.coref
   mangoes.modeling.training_utils

Module contents
---------------

.. automodule:: mangoes.modeling
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
