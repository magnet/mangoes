import pytest
from transformers import pipeline, SquadExample, BertConfig
from mangoes.modeling import *

ERNIE_BASE_TOKENIZER = "bert-base-uncased"
ERNIE_TINY_MODEL = "hf-internal-testing/tiny-random-bert"

dummy_entity_vectors = np.zeros(shape=(20, 100))
ernie_config = BertConfig(**{
    "attention_probs_dropout_prob": 0.1,
    "hidden_act": "gelu",
    "hidden_dropout_prob": 0.1,
    "hidden_size": 32,
    "initializer_range": 0.02,
    "intermediate_size": 37,
    "max_position_embeddings": 512,
    "num_attention_heads": 4,
    "num_hidden_layers": 5,
    "type_vocab_size": 2,
    "vocab_size": 1124,
    "layer_types": ["sim", "sim", "mix", "norm", "norm"],
    "num_labels": 2
})


def save_tokenizer_dicts_to_file(save_directory, entity_name_to_id, entity_id_to_index):
    remove_chars = len(os.linesep)
    with open(f"{save_directory}/entity_map.txt", 'a+') as f:
        for key, value in entity_name_to_id.items():
            f.write(f"{key}\t{value}\n")
        f.truncate(f.tell() - remove_chars)
    with open(f"{save_directory}/entity2id.txt", 'a+') as f:
        f.write("\n")  # blank line to get same format as pretrained ernie mapping
        for key, value in entity_id_to_index.items():
            f.write(f"{key}\t{value}\n")
        f.truncate(f.tell() - remove_chars)


@pytest.mark.unittest
def test_ernie_tokenizer_save_load(entity_annotation_example_data, tmpdir_factory):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_BASE_TOKENIZER,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    assert "entities" in tokenizer.model_input_names
    assert "entity_mask" in tokenizer.model_input_names
    assert tokenizer.entity_name_to_id == {"test": 'Q1132113', "china": 'Q130582'}
    assert tokenizer.entity_id_to_index == {'Q1132113': 1, "Q130582": 2}
    save_dir = tmpdir_factory.mktemp('tokenizer_save')

    tokenizer.save_pretrained(str(save_dir))
    loaded_tokenizer = ErnieTokenizer.from_pretrained(str(save_dir),
                                                      entity_map_filepath=str(save_dir) + "/entity_map.txt",
                                                      entity2id_filepath=str(save_dir) + "/entity2id.txt"
                                                      )
    assert loaded_tokenizer.entity_name_to_id == {"test": 'Q1132113', "china": 'Q130582'}
    assert loaded_tokenizer.entity_id_to_index == {'Q1132113': 1, "Q130582": 2}
    assert "entities" in tokenizer.model_input_names
    assert "entity_mask" in tokenizer.model_input_names


@pytest.mark.unittest
def test_ernie_tokenizer_process_tagme_annotations(entity_annotation_example_data, tagme_annotations, tmpdir_factory):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_BASE_TOKENIZER,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    ann_response, text, output = tagme_annotations
    process_annotations = tokenizer.process_entity_annotations(ann_response, text)
    assert all(tuple(process_annotations[i]) == tuple(output[i]) for i in range(len(process_annotations)))


@pytest.mark.unittest
def test_ernie_tokenizer_call_single(entity_annotation_example_data, tmpdir_factory):
    text, text_pair, text_annotations, text_pair_annotations, entity_id_sequence, entity_mask, entity_name_to_id, \
        entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_BASE_TOKENIZER,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    tokenizer_output = tokenizer(text, text_pair, text_annotations=text_annotations,
                                 text_pair_annotations=text_pair_annotations, return_tensors="pt")
    assert torch.tensor(tokenizer_output["entities"]).shape == torch.tensor(tokenizer_output["entity_mask"]).shape == \
           torch.tensor(tokenizer_output["input_ids"]).shape == torch.tensor(tokenizer_output["attention_mask"]).shape
    assert tokenizer_output["entities"].tolist() == [entity_id_sequence]
    assert tokenizer_output["entity_mask"].tolist() == [entity_mask]


@pytest.mark.unittest
def test_ernie_tokenizer_call_batched(entity_annotation_example_data, tmpdir_factory):
    text, text_pair, text_annotations, text_pair_annotations, entity_id_sequence, entity_mask, entity_name_to_id, \
        entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_BASE_TOKENIZER,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    tokenizer_output = tokenizer([text], [text_pair], text_annotations=[text_annotations],
                                 text_pair_annotations=[text_pair_annotations], return_tensors="pt")
    assert tokenizer_output["entities"].shape == tokenizer_output["entity_mask"].shape == \
           tokenizer_output["input_ids"].shape == tokenizer_output["attention_mask"].shape
    assert tokenizer_output["entities"].tolist() == [entity_id_sequence]
    assert tokenizer_output["entity_mask"].tolist() == [entity_mask]


@pytest.mark.unittest
def test_ernie_tokenizer_call_single_split_words(entity_annotation_example_data, tmpdir_factory):
    text, text_pair, text_annotations, text_pair_annotations, entity_id_sequence, entity_mask, entity_name_to_id, \
        entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_BASE_TOKENIZER,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    tokenizer_output = tokenizer(text.split(), text_pair.split(), is_split_into_words=True,
                                 text_annotations=text_annotations, text_pair_annotations=text_pair_annotations)
    assert torch.tensor(tokenizer_output["entities"]).shape == torch.tensor(tokenizer_output["entity_mask"]).shape == \
           torch.tensor(tokenizer_output["input_ids"]).shape == torch.tensor(tokenizer_output["attention_mask"]).shape
    assert tokenizer_output["entities"] == entity_id_sequence
    assert tokenizer_output["entity_mask"] == entity_mask


@pytest.mark.unittest
def test_ernie_tokenizer_call_batched_split_words(entity_annotation_example_data, tmpdir_factory):
    text, text_pair, text_annotations, text_pair_annotations, entity_id_sequence, entity_mask, entity_name_to_id, \
        entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_BASE_TOKENIZER,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    tokenizer_output = tokenizer([text.split()], [text_pair.split()], is_split_into_words=True,
                                 text_annotations=[text_annotations], text_pair_annotations=[text_pair_annotations])
    assert torch.tensor(tokenizer_output["entities"]).shape == torch.tensor(tokenizer_output["entity_mask"]).shape == \
           torch.tensor(tokenizer_output["input_ids"]).shape == torch.tensor(tokenizer_output["attention_mask"]).shape
    assert tokenizer_output["entities"]  == [entity_id_sequence]
    assert tokenizer_output["entity_mask"] == [entity_mask]


@pytest.mark.unittest
def test_ernie_base_transformer_outputs(tmpdir_factory, entity_annotation_example_data, raw_small_source_string):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    ernie_model = ErnieModel(ernie_config, kg_embeddings=dummy_entity_vectors)
    pretrained_model = TransformerModel(ernie_model, tokenizer)
    outputs = pretrained_model.generate_outputs(raw_small_source_string, pre_tokenized=False, output_hidden_states=True,
                                                output_attentions=True)
    assert len(outputs["hidden_states"]) == pretrained_model.model.config.num_hidden_layers + 1


@pytest.mark.unittest
def test_ernie_feature_extraction_predict(tmpdir_factory, entity_annotation_example_data, raw_small_source_string):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_dir)+"/entity2id.txt")
    ernie_model = ErnieModel(ernie_config, kg_embeddings=dummy_entity_vectors)
    pretrained_mod = TransformerForFeatureExtraction(ernie_model, tokenizer)
    outputs = pretrained_mod.predict(raw_small_source_string)
    extraction_pipeline = pipeline('feature-extraction', model=ernie_model, tokenizer=tokenizer)
    pipeline_emb = extraction_pipeline(raw_small_source_string)
    assert np.array_equal(np.array(pipeline_emb[0]), np.array(outputs[0]))
    assert np.array_equal(np.array(pipeline_emb[1]), np.array(outputs[1]))


@pytest.mark.unittest
def test_ernie_seqclassifier_save_load(raw_small_source_string, tmpdir_factory, entity_annotation_example_data):
    output_dir = tmpdir_factory.mktemp('model')
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir)+"/entity2id.txt")
    ernie_model = ErnieForSequenceClassification(ernie_config, kg_embeddings=dummy_entity_vectors)
    seq_classifier_model = TransformerForSequenceClassification(ernie_model, tokenizer)
    seq_classifier_model.train(train_text=raw_small_source_string, train_targets=[0, 1], output_dir=output_dir,
                               num_train_epochs=1, report_to="none")
    seq_classifier_model.save(output_dir, save_tokenizer=True)
    outputs = seq_classifier_model.generate_outputs(raw_small_source_string)
    tokenizer = ErnieTokenizer.from_pretrained(str(output_dir),
                                               entity_map_filepath=str(output_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(output_dir)+"/entity2id.txt")
    ernie_model = ErnieForSequenceClassification.from_pretrained(str(output_dir), kg_embeddings=dummy_entity_vectors,
                                                                 labels=["pos", "neg"])
    loaded_model = TransformerForSequenceClassification(ernie_model, tokenizer)
    outputs2 = loaded_model.generate_outputs(raw_small_source_string)
    assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_ernie_seqclassifier_predict(tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir)+"/entity2id.txt")
    ernie_model = ErnieForSequenceClassification(ernie_config, kg_embeddings=dummy_entity_vectors)
    seq_classifier_model = TransformerForSequenceClassification(ernie_model, tokenizer)
    predictions = seq_classifier_model.predict("this is a test sentence")
    assert 0.0 <= predictions[0]["score"] <= 1.0


@pytest.mark.unittest
def test_ernie_tokenclassifier_save_load(raw_small_source_string, tmpdir_factory, entity_annotation_example_data):
    output_dir = tmpdir_factory.mktemp('model')
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir)+"/entity2id.txt")
    ernie_model = ErnieForTokenClassification(ernie_config, kg_embeddings=dummy_entity_vectors)
    token_classifier_model = TransformerForTokenClassification(ernie_model, tokenizer)
    token_classifier_model.train(train_text=[raw_small_source_string[0].split(), raw_small_source_string[1].split()],
                                 train_targets=[[1, 0, 1, 0], [0, 1, 0]],
                                 output_dir=output_dir, num_train_epochs=1, report_to="none")
    token_classifier_model.save(output_dir, save_tokenizer=True)
    outputs = token_classifier_model.generate_outputs(raw_small_source_string)
    tokenizer = ErnieTokenizer.from_pretrained(str(output_dir),
                                               entity_map_filepath=str(output_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(output_dir)+"/entity2id.txt")
    ernie_model = ErnieForTokenClassification.from_pretrained(str(output_dir), kg_embeddings=dummy_entity_vectors,
                                                              labels=["pos", "neg"])
    loaded_model = TransformerForTokenClassification(ernie_model, tokenizer)
    outputs2 = loaded_model.generate_outputs(raw_small_source_string)
    assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_ernie_tokenclassifier_predict(tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir)+"/entity2id.txt")
    ernie_model = ErnieForTokenClassification(ernie_config, kg_embeddings=dummy_entity_vectors)
    token_classifier_model = TransformerForTokenClassification(ernie_model, tokenizer, labels=["pos", "neg"])
    predictions = token_classifier_model.predict("this is a test sentence")
    assert 0.0 <= predictions[0][0]["score"] <= 1.0
    assert predictions[0][0]["word"][0] == "t"


@pytest.mark.unittest
def test_ernie_questionanswer_save_load(question_answering_data, tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir)+"/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir)+"/entity2id.txt")
    ernie_model = ErnieForQuestionAnswering(ernie_config, kg_embeddings=dummy_entity_vectors)
    question_answer_model = TransformerForQuestionAnswering(ernie_model, tokenizer)
    output_dir = tmpdir_factory.mktemp('model')
    question_answer_model.train(train_question_texts=question_answering_data[0],
                                train_context_texts=question_answering_data[1],
                                train_answer_texts=question_answering_data[2],
                                train_start_indices=question_answering_data[3],
                                output_dir=output_dir, num_train_epochs=1, report_to="none",
                                max_seq_length=question_answer_model.tokenizer.model_max_length,
                                max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                doc_stride=int(question_answer_model.tokenizer.model_max_length / 3)
                                )
    save_dir = tmpdir_factory.mktemp('data')
    question_answer_model.save(save_dir, save_tokenizer=False)
    outputs = question_answer_model.generate_outputs(question=question_answering_data[0],
                                                     context=question_answering_data[1],
                                                     pre_tokenized=False, output_hidden_states=False,
                                                     output_attentions=False)
    ernie_model = ErnieForQuestionAnswering.from_pretrained(str(save_dir), kg_embeddings=dummy_entity_vectors)
    loaded_model = TransformerForQuestionAnswering(ernie_model, tokenizer)
    outputs2 = loaded_model.generate_outputs(question=question_answering_data[0],
                                             context=question_answering_data[1],
                                             pre_tokenized=False, output_hidden_states=False,
                                             output_attentions=False)
    assert np.array_equal(outputs["start_logits"].numpy(), outputs2["start_logits"].numpy(), )
    assert np.array_equal(outputs["end_logits"].numpy(), outputs2["end_logits"].numpy(), )


@pytest.mark.unittest
def test_ernie_questionanswer_predict_strings(question_answering_data, tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir) + "/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir) + "/entity2id.txt")
    ernie_model = ErnieForQuestionAnswering(ernie_config, kg_embeddings=dummy_entity_vectors)
    question_answer_model = TransformerForQuestionAnswering(ernie_model, tokenizer)
    predictions = question_answer_model.predict(question=question_answering_data[0][0],
                                                context=question_answering_data[1][0])
    assert 0.0 <= predictions[0]["score"] <= 1.0
    assert predictions[0]["start"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])
    assert predictions[0]["end"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])


@pytest.mark.unittest
def test_ernie_questionanswer_predict_squadexample(question_answering_data, tmpdir_factory,
                                                   entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir) + "/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir) + "/entity2id.txt")
    ernie_model = ErnieForQuestionAnswering(ernie_config, kg_embeddings=dummy_entity_vectors)
    question_answer_model = TransformerForQuestionAnswering(ernie_model, tokenizer)
    example = SquadExample(qas_id=0, question_text=question_answering_data[0][0],
                           context_text=question_answering_data[1][0], start_position_character=0,
                           title="", answer_text=question_answering_data[2][0])
    predictions = question_answer_model.predict(example)
    assert 0.0 <= predictions[0]["score"] <= 1.0
    assert predictions[0]["start"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])
    assert predictions[0]["end"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])


@pytest.mark.unittest
def test_ernie_multiplechoice_save_load(multiple_choice_example, tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir) + "/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir) + "/entity2id.txt")
    ernie_model = ErnieForMultipleChoice(ernie_config, kg_embeddings=dummy_entity_vectors)
    multiple_choice_model = TransformerForMultipleChoice(ernie_model, tokenizer)
    output_dir = tmpdir_factory.mktemp('model')
    multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                train_choices_texts=[multiple_choice_example[1]] * 5,
                                train_labels=[0] * 5,
                                per_device_train_batch_size=2,
                                output_dir=output_dir, num_train_epochs=1, report_to="none")
    save_dir = tmpdir_factory.mktemp('data')
    multiple_choice_model.save(save_dir, save_tokenizer=False)
    outputs = multiple_choice_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                                     choices=[multiple_choice_example[1]] * 2,
                                                     pre_tokenized=False, output_hidden_states=False,
                                                     output_attentions=False)
    ernie_model = ErnieForMultipleChoice.from_pretrained(str(save_dir), kg_embeddings=dummy_entity_vectors)
    loaded_model = TransformerForMultipleChoice(ernie_model, tokenizer)
    outputs2 = loaded_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                             choices=[multiple_choice_example[1]] * 2,
                                             pre_tokenized=False, output_hidden_states=False,
                                             output_attentions=False)
    assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_ernie_multiplechoice_predict(multiple_choice_example, tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir) + "/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir) + "/entity2id.txt")
    ernie_model = ErnieForMultipleChoice(ernie_config, kg_embeddings=dummy_entity_vectors)
    multiple_choice_model = TransformerForMultipleChoice(ernie_model, tokenizer)
    predictions = multiple_choice_model.predict(questions=multiple_choice_example[0],
                                                choices=multiple_choice_example[1])
    assert 0.0 <= predictions[0]["score"] <= 1.0


@pytest.mark.unittest
def test_ernie_coref_train_save_load(raw_coref_data, tmpdir_factory, entity_annotation_example_data):
    _, _, _, _, _, _, entity_name_to_id, entity_id_to_index = entity_annotation_example_data
    save_tok_dir = tmpdir_factory.mktemp('tokenizer')
    save_tokenizer_dicts_to_file(str(save_tok_dir), entity_name_to_id, entity_id_to_index)
    tokenizer = ErnieTokenizer.from_pretrained(ERNIE_TINY_MODEL,
                                               entity_map_filepath=str(save_tok_dir) + "/entity_map.txt",
                                               entity2id_filepath=str(save_tok_dir) + "/entity2id.txt")
    ernie_model = ErnieModel(ernie_config, kg_embeddings=dummy_entity_vectors)

    mod = TransformerForCoreferenceResolution(ernie_model, tokenizer, max_span_width=1, top_span_ratio=1.0,
                                              ffnn_hidden_size=100,
                                              use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                              intermediate_size=256)
    mod_path = tmpdir_factory.mktemp('mod_path')
    coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
    max_segment_len = 15
    mod.train(mod_path, max_segment_len=max_segment_len, max_segments=5, task_learn_rate=None,
              train_documents=coref_documents, train_cluster_ids=coref_cluster_ids,
              train_speaker_ids=coref_speakers,
              train_genres=coref_genres, eval_documents=coref_documents, eval_cluster_ids=coref_cluster_ids,
              eval_speaker_ids=coref_speakers, eval_genres=coref_genres, num_train_epochs=1, report_to="none")
    save_dir = tmpdir_factory.mktemp('data')
    mod.save(save_dir, save_tokenizer=False)
    results = mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre=1)
    loaded_model = TransformerForCoreferenceResolution(str(save_dir), tokenizer, pretrained=True)
    outputs2 = loaded_model.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0],
                                             genre=1)
    assert np.array_equal(mod.model.features_model.embeddings.word_embeddings.weight.detach().numpy(),
                          loaded_model.model.features_model.embeddings.word_embeddings.weight.detach().numpy())
    assert np.array_equal(mod.model.mention_scorer[0].weight.detach().numpy(),
                          loaded_model.model.mention_scorer[0].weight.detach().numpy())
    assert np.array_equal(results["candidate_starts"].numpy(), outputs2["candidate_starts"].numpy(), )
    assert np.array_equal(results["candidate_ends"].numpy(), outputs2["candidate_ends"].numpy(), )
    assert np.array_equal(results["candidate_mention_scores"].numpy(),
                          outputs2["candidate_mention_scores"].numpy(), )
    assert np.array_equal(results["top_span_starts"].numpy(), outputs2["top_span_starts"].numpy(), )
    assert np.array_equal(results["top_span_ends"].numpy(), outputs2["top_span_ends"].numpy(), )
    assert np.array_equal(results["top_antecedent_scores"].numpy(), outputs2["top_antecedent_scores"].numpy(), )
    assert np.array_equal(results["top_antecedents"].numpy(), outputs2["top_antecedents"].numpy(), )
