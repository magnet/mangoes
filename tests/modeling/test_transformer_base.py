# -*- coding: utf-8 -*-
import logging

import pytest
from transformers import AutoModel, AutoModelForMaskedLM, AutoTokenizer, pipeline
import numpy as np

from mangoes.modeling import merge_subword_embeddings, TransformerModel

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)

TINY_MODELS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-gpt2",
               "hf-internal-testing/tiny-random-bart", "hf-internal-testing/tiny-random-electra",
               "hf-internal-testing/tiny-random-distilbert"]

TINY_MODELS_FASTTOK = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-electra",
                       "hf-internal-testing/tiny-random-distilbert"]

SMALL_SOURCE_STRING_SUBTOKENS = {"hf-internal-testing/tiny-random-bert": 20, "hf-internal-testing/tiny-random-gpt2": 10,
                                 "hf-internal-testing/tiny-random-bart": 12,
                                 "hf-internal-testing/tiny-random-electra": 20,
                                 "hf-internal-testing/tiny-random-distilbert": 20}

TINY_MODEL_MLM_CLASS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-electra",
                        "hf-internal-testing/tiny-random-distilbert"]

sub_word_indices = {"hf-internal-testing/tiny-random-bert": [[1, 4], [4, 5], [5, 8], [8, 16]],
                    "hf-internal-testing/tiny-random-gpt2": [[0, 3], [3, 4], [4, 5], [5, 8]],
                    "hf-internal-testing/tiny-random-bart": [[1, 4], [4, 5], [5, 6], [6, 9]],
                    "hf-internal-testing/tiny-random-electra": [[1, 4], [4, 5], [5, 8], [8, 16]],
                    "hf-internal-testing/tiny-random-distilbert": [[1, 4], [4, 5], [5, 8], [8, 16]]}


@pytest.mark.unittest
def test_base_transformer_maskedlm_outputs(raw_small_source_string):
    for model_name in TINY_MODEL_MLM_CLASS:
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        model = AutoModelForMaskedLM.from_pretrained(model_name)
        pretrained_maskedlm_model = TransformerModel(model, tokenizer)
        outputs = pretrained_maskedlm_model.generate_outputs(raw_small_source_string, pre_tokenized=False,
                                                             output_hidden_states=True,
                                                             output_attentions=True)
        n_tokens = SMALL_SOURCE_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["logits"].shape) == (2, n_tokens, pretrained_maskedlm_model.model.config.vocab_size)
        assert len(outputs["attentions"]) == pretrained_maskedlm_model.model.config.num_hidden_layers
        assert tuple(outputs["attentions"][0].shape) == (2, pretrained_maskedlm_model.model.config.num_attention_heads,
                                                         n_tokens, n_tokens)
        assert len(outputs["hidden_states"]) == pretrained_maskedlm_model.model.config.num_hidden_layers + 1
        assert tuple(outputs["hidden_states"][0].shape) == (2, n_tokens,
                                                            pretrained_maskedlm_model.model.config.hidden_size)


@pytest.mark.unittest
def test_subword_merging_raw(raw_small_source_string):
    for model_name in TINY_MODELS:
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        raw_outputs = np.array(extraction_pipeline(raw_small_source_string[0]))
        offset_mapping = tokenizer(raw_small_source_string[0], is_split_into_words=False, return_offsets_mapping=True,
                                   padding=True, return_tensors='pt').pop('offset_mapping').numpy()
        word_embeddings = merge_subword_embeddings(raw_outputs, raw_small_source_string[0], offset_mapping)
        for i in range(4):
            assert np.array_equal(np.mean(raw_outputs[0][sub_word_indices[model_name][i][0]:
                                                         sub_word_indices[model_name][i][1]][:], 0),
                                  word_embeddings[0][i])


@pytest.mark.unittest
def test_subword_merging_presplit(raw_small_source_string):
    for model_name in TINY_MODELS_FASTTOK:
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        raw_outputs = np.array(extraction_pipeline(raw_small_source_string[0]))
        offset_mapping = tokenizer(raw_small_source_string[0].split(), is_split_into_words=True,
                                   return_offsets_mapping=True, padding=True, return_tensors='pt').pop(
            'offset_mapping').numpy()
        word_embeddings = merge_subword_embeddings(raw_outputs, raw_small_source_string[0].split(), offset_mapping,
                                                   pretokenized=True)
        for i in range(4):
            assert np.array_equal(np.mean(raw_outputs[0][sub_word_indices[model_name][i][0]:
                                                         sub_word_indices[model_name][i][1]][:], 0),
                                  word_embeddings[0][i])
