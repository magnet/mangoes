import pytest

from mangoes.modeling import *
from transformers import pipeline

LIBERT_TEST_MODEL = "hf-tiny-model-private/tiny-random-BertModel"


@pytest.mark.unittest
def test_libert_base_transformer_outputs(raw_small_source_string):
    libert_model = LibertModel.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    pretrained_model = TransformerModel(libert_model, libert_tokenizer)
    outputs = pretrained_model.generate_outputs(raw_small_source_string, pre_tokenized=False, output_hidden_states=True,
                                                output_attentions=True)
    assert len(outputs["attentions"]) == pretrained_model.model.config.num_hidden_layers
    assert tuple(outputs["attentions"][0].shape) == (2, pretrained_model.model.config.num_attention_heads, 20, 20)
    assert len(outputs["hidden_states"]) == pretrained_model.model.config.num_hidden_layers + 1


@pytest.mark.unittest
def test_libert_feature_extraction_predict(raw_small_source_string):
    libert_model = LibertModel.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    pretrained_mod = TransformerForFeatureExtraction(libert_model, libert_tokenizer)
    outputs = pretrained_mod.predict(raw_small_source_string)
    extraction_pipeline = pipeline('feature-extraction', model=libert_model, tokenizer=LIBERT_TEST_MODEL)
    pipeline_emb = extraction_pipeline(raw_small_source_string)
    assert np.array_equal(np.array(pipeline_emb[0]), np.array(outputs[0]))
    assert np.array_equal(np.array(pipeline_emb[1]), np.array(outputs[1]))


@pytest.mark.unittest
def test_libert_seqclassifier_save_load(raw_small_source_string, tmpdir_factory):
    output_dir = tmpdir_factory.mktemp('model')
    save_dir = tmpdir_factory.mktemp('data')
    libert_model = LibertForSequenceClassification.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL,
                                                                   labels=["pos", "neg"])
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    seq_classifier_model = TransformerForSequenceClassification(libert_model, libert_tokenizer)
    seq_classifier_model.train(train_text=raw_small_source_string, train_targets=[0, 1], output_dir=output_dir,
                               num_train_epochs=1, report_to="none")
    seq_classifier_model.save(save_dir, save_tokenizer=False)
    outputs = seq_classifier_model.generate_outputs(raw_small_source_string)
    loaded_model = TransformerForSequenceClassification(str(save_dir), LIBERT_TEST_MODEL,
                                                        labels=["pos", "neg"])
    outputs2 = loaded_model.generate_outputs(raw_small_source_string)
    assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_libert_seqclassifier_predict():
    libert_model = LibertForSequenceClassification.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL,
                                                                   labels=["pos", "neg"])
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    seq_classifier_model = TransformerForSequenceClassification(libert_model, libert_tokenizer)
    predictions = seq_classifier_model.predict("this is a test sentence")
    assert 0.0 <= predictions[0]["score"] <= 1.0


@pytest.mark.unittest
def test_libert_tokenclassifier_save_load(raw_source_string, tmpdir_factory):
    output_dir = tmpdir_factory.mktemp('model')
    save_dir = tmpdir_factory.mktemp('data')
    libert_model = LibertForTokenClassification.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL,
                                                                labels=["pos", "neg"])
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    token_classifier_model = TransformerForTokenClassification(libert_model, libert_tokenizer)
    token_classifier_model.train(train_text=[raw_source_string[0].split()], train_targets=[[1, 0, 1, 0, 1]],
                                 output_dir=output_dir, num_train_epochs=1, report_to="none")
    token_classifier_model.save(save_dir, save_tokenizer=False)
    outputs = token_classifier_model.generate_outputs(raw_source_string)
    loaded_model = TransformerForTokenClassification(str(save_dir), LIBERT_TEST_MODEL,
                                                     labels=["pos", "neg"])
    outputs2 = loaded_model.generate_outputs(raw_source_string)
    assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_libert_tokenclassifier_predict():
    libert_model = LibertForTokenClassification.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL,
                                                                labels=["pos", "neg"])
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    token_classifier_model = TransformerForTokenClassification(libert_model, libert_tokenizer)
    predictions = token_classifier_model.predict("this is a test sentence")
    assert 0.0 <= predictions[0][0]["score"] <= 1.0
    assert predictions[0][0]["word"][0] == "t"
    assert predictions[0][0]["entity"] in ["pos", "neg"]


@pytest.mark.unittest
def test_libert_questionanswer_save_load(question_answering_data, tmpdir_factory):
    libert_model = LibertForQuestionAnswering.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizer.from_pretrained(LIBERT_TEST_MODEL)
    question_answer_model = TransformerForQuestionAnswering(libert_model, libert_tokenizer)
    output_dir = tmpdir_factory.mktemp('model')
    question_answer_model.train(train_question_texts=question_answering_data[0],
                                train_context_texts=question_answering_data[1],
                                train_answer_texts=question_answering_data[2],
                                train_start_indices=question_answering_data[3],
                                output_dir=output_dir, num_train_epochs=1, report_to="none",
                                max_seq_length=question_answer_model.tokenizer.model_max_length,
                                max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                doc_stride=int(question_answer_model.tokenizer.model_max_length / 3)
                                )
    save_dir = tmpdir_factory.mktemp('data')
    question_answer_model.save(save_dir, save_tokenizer=False)
    outputs = question_answer_model.generate_outputs(question=question_answering_data[0],
                                                     context=question_answering_data[1],
                                                     pre_tokenized=False, output_hidden_states=False,
                                                     output_attentions=False)
    loaded_model = TransformerForQuestionAnswering(str(save_dir), LIBERT_TEST_MODEL)
    outputs2 = loaded_model.generate_outputs(question=question_answering_data[0],
                                             context=question_answering_data[1],
                                             pre_tokenized=False, output_hidden_states=False,
                                             output_attentions=False)
    assert np.array_equal(outputs["start_logits"].numpy(), outputs2["start_logits"].numpy(), )
    assert np.array_equal(outputs["end_logits"].numpy(), outputs2["end_logits"].numpy(), )


@pytest.mark.unittest
def test_libert_questionanswer_predict_strings(question_answering_data):
    libert_model = LibertForQuestionAnswering.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizer.from_pretrained(LIBERT_TEST_MODEL)
    pretrained_mod = TransformerForQuestionAnswering(libert_model, libert_tokenizer)
    predictions = pretrained_mod.predict(question=question_answering_data[0][0],
                                         context=question_answering_data[1][0])
    assert 0.0 <= predictions[0]["score"] <= 1.0
    assert predictions[0]["start"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])
    assert predictions[0]["end"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])


@pytest.mark.unittest
def test_libert_multiplechoice_save_load(multiple_choice_example, tmpdir_factory):
    libert_model = LibertForMultipleChoice.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    multiple_choice_model = TransformerForMultipleChoice(libert_model, libert_tokenizer)
    output_dir = tmpdir_factory.mktemp('model')
    multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                train_choices_texts=[multiple_choice_example[1]] * 5,
                                train_labels=[0] * 5,
                                per_device_train_batch_size=2,
                                output_dir=output_dir, num_train_epochs=1, report_to="none")
    save_dir = tmpdir_factory.mktemp('data')
    multiple_choice_model.save(save_dir, save_tokenizer=False)
    outputs = multiple_choice_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                                     choices=[multiple_choice_example[1]] * 2,
                                                     pre_tokenized=False, output_hidden_states=False,
                                                     output_attentions=False)
    loaded_model = TransformerForMultipleChoice(str(save_dir), LIBERT_TEST_MODEL)
    outputs2 = loaded_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                             choices=[multiple_choice_example[1]] * 2,
                                             pre_tokenized=False, output_hidden_states=False,
                                             output_attentions=False)
    assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_libert_multiplechoice_predict(multiple_choice_example):
    libert_model = LibertForMultipleChoice.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    multiple_choice_model = TransformerForMultipleChoice(libert_model, libert_tokenizer)
    predictions = multiple_choice_model.predict(questions=multiple_choice_example[0],
                                                choices=multiple_choice_example[1])
    assert 0.0 <= predictions[0]["score"] <= 1.0


@pytest.mark.unittest
def test_libert_coref_train_save_load(raw_coref_data, tmpdir_factory):
    libert_model = LibertModel.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    mod = TransformerForCoreferenceResolution(libert_model, libert_tokenizer, max_span_width=1,
                                              top_span_ratio=1.0,
                                              ffnn_hidden_size=100,
                                              use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                              intermediate_size=256)
    mod_path = tmpdir_factory.mktemp('mod_path')
    coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
    max_segment_len = 15
    mod.train(mod_path, max_segment_len=max_segment_len, max_segments=5, task_learn_rate=None,
              train_documents=coref_documents, train_cluster_ids=coref_cluster_ids,
              train_speaker_ids=coref_speakers,
              train_genres=coref_genres, eval_documents=coref_documents, eval_cluster_ids=coref_cluster_ids,
              eval_speaker_ids=coref_speakers, eval_genres=coref_genres, num_train_epochs=1, report_to="none")
    save_dir = tmpdir_factory.mktemp('data')
    mod.save(save_dir, save_tokenizer=False)
    results = mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre=1)
    loaded_model = TransformerForCoreferenceResolution(str(save_dir), LIBERT_TEST_MODEL, pretrained=True)
    outputs2 = loaded_model.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0],
                                             genre=1)
    assert np.array_equal(mod.model.features_model.embeddings.word_embeddings.weight.detach().numpy(),
                          loaded_model.model.features_model.embeddings.word_embeddings.weight.detach().numpy())
    assert np.array_equal(mod.model.mention_scorer[0].weight.detach().numpy(),
                          loaded_model.model.mention_scorer[0].weight.detach().numpy())
    assert np.array_equal(results["candidate_starts"].numpy(), outputs2["candidate_starts"].numpy(), )
    assert np.array_equal(results["candidate_ends"].numpy(), outputs2["candidate_ends"].numpy(), )
    assert np.array_equal(results["candidate_mention_scores"].numpy(),
                          outputs2["candidate_mention_scores"].numpy(), )
    assert np.array_equal(results["top_span_starts"].numpy(), outputs2["top_span_starts"].numpy(), )
    assert np.array_equal(results["top_span_ends"].numpy(), outputs2["top_span_ends"].numpy(), )
    assert np.array_equal(results["top_antecedent_scores"].numpy(), outputs2["top_antecedent_scores"].numpy(), )
    assert np.array_equal(results["top_antecedents"].numpy(), outputs2["top_antecedents"].numpy(), )


@pytest.mark.unittest
def test_libert_coref_predict(raw_coref_data):
    libert_model = LibertModel.from_pretrained(pretrained_model_name_or_path=LIBERT_TEST_MODEL)
    libert_tokenizer = LibertTokenizerFast.from_pretrained(LIBERT_TEST_MODEL)
    mod = TransformerForCoreferenceResolution(libert_model, libert_tokenizer, max_span_width=1,
                                              top_span_ratio=1.0,
                                              ffnn_hidden_size=100,
                                              use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                              intermediate_size=256)
    coref_documents, _, coref_speakers, _ = raw_coref_data
    results = mod.predict(coref_documents[0][0], pre_tokenized=True, speaker_ids=coref_speakers[0][0], genre=1)
    for cluster_dict in results:
        assert len(cluster_dict["cluster_ids"]) == len(cluster_dict["cluster_tokens"])
