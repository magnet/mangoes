# -*- coding: utf-8 -*-
import logging
import pytest
from transformers import SquadExample, TrainingArguments, AutoModel, AutoTokenizer, pipeline
import numpy as np

from mangoes.modeling import TransformerForSequenceClassification, TransformerForTokenClassification, \
    TransformerForQuestionAnswering, TransformerForCoreferenceResolution, MangoesCoreferenceDataset, \
    CoreferenceFineTuneTrainer, TransformerForMultipleChoice, TransformerForFeatureExtraction

logging_level = logging.WARNING
logging_format = '%(asctime)s :: %(name)s::%(funcName)s() :: %(levelname)s :: %(message)s'
logging.basicConfig(level=logging_level, format=logging_format)  # , filename="report.log")
logger = logging.getLogger(__name__)
TINY_MODEL_MAXLENGTHS = {"hf-internal-testing/tiny-random-gpt2": 512, "hf-internal-testing/tiny-random-bart": 100}
SMALL_SOURCE_STRING_SUBTOKENS = {"hf-internal-testing/tiny-random-bert": 20, "hf-internal-testing/tiny-random-gpt2": 10,
                                 "hf-internal-testing/tiny-random-bart": 12,
                                 "hf-internal-testing/tiny-random-electra": 20,
                                 "hf-internal-testing/tiny-random-distilbert": 20}
QA_STRING_SUBTOKENS = {"hf-internal-testing/tiny-random-bert": 68, "hf-internal-testing/tiny-random-bart": 35,
                       "hf-internal-testing/tiny-random-electra": 68, "hf-internal-testing/tiny-random-distilbert": 68}
MC_STRING_SUBTOKENS = {"hf-internal-testing/tiny-random-bert": 107,
                       "hf-internal-testing/tiny-random-electra": 107,
                       "hf-internal-testing/tiny-random-distilbert": 107}
TINY_MODEL_FECLASS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-gpt2",
                      "hf-internal-testing/tiny-random-bart", "hf-internal-testing/tiny-random-electra",
                      "hf-internal-testing/tiny-random-distilbert"]
TINY_MODEL_FECLASS_PRETOKENIZED = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-electra",
                                   "hf-internal-testing/tiny-random-distilbert"]
TINY_MODEL_SEQCLASS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-gpt2",
                       "hf-internal-testing/tiny-random-bart", "hf-internal-testing/tiny-random-electra",
                       "hf-internal-testing/tiny-random-distilbert"]
TINY_MODEL_TOKCLASS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-gpt2",
                       "hf-internal-testing/tiny-random-electra", "hf-internal-testing/tiny-random-distilbert"]
TINY_MODEL_QACLASS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-bart",
                      "hf-internal-testing/tiny-random-electra", "hf-internal-testing/tiny-random-distilbert"]
TINY_MODEL_MCCLASS = ["hf-internal-testing/tiny-random-bert", "hf-internal-testing/tiny-random-electra",
                      "hf-internal-testing/tiny-random-distilbert"]
TINY_MODEL_COREFCLASS = ["hf-internal-testing/tiny-random-bert",
                         "hf-internal-testing/tiny-random-electra",
                         "hf-internal-testing/tiny-random-distilbert"]

sub_word_indices_raw_small_string = {"hf-internal-testing/tiny-random-bert": [[1, 4], [4, 5], [5, 8], [8, 16]],
                                     "hf-internal-testing/tiny-random-gpt2": [[0, 3], [3, 4], [4, 5], [5, 8]],
                                     "hf-internal-testing/tiny-random-bart": [[1, 4], [4, 5], [5, 6], [6, 9]],
                                     "hf-internal-testing/tiny-random-electra": [[1, 4], [4, 5], [5, 8], [8, 16]],
                                     "hf-internal-testing/tiny-random-distilbert": [[1, 4], [4, 5], [5, 8], [8, 16]]}


@pytest.mark.unittest
def test_feature_extraction_generate_output_single_sentence(raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(raw_small_source_string[0], pre_tokenized=False,
                                                  output_hidden_states=True, output_attentions=True)
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        pipeline_emb = extraction_pipeline(raw_small_source_string[0])
        if "hidden_states" in outputs:
            assert np.array_equal(np.array(pipeline_emb), outputs["hidden_states"][-1].cpu().numpy())
        else:
            assert np.array_equal(np.array(pipeline_emb), outputs["decoder_hidden_states"][-1].cpu().numpy())


@pytest.mark.unittest
def test_feature_extraction_generate_output_multi_sentence(raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(raw_small_source_string, pre_tokenized=False,
                                                  output_hidden_states=True, output_attentions=True)
        seq_length = SMALL_SOURCE_STRING_SUBTOKENS[model_name]
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        pipeline_emb = extraction_pipeline(raw_small_source_string)
        if "attentions" in outputs:
            assert len(outputs["attentions"]) == pretrained_mod.model.config.num_hidden_layers
            assert tuple(outputs["attentions"][0].shape) == (
                2, pretrained_mod.model.config.num_attention_heads, seq_length, seq_length)
        else:
            assert len(outputs["cross_attentions"]) == pretrained_mod.model.config.num_hidden_layers
            assert tuple(outputs["cross_attentions"][0].shape) == (
                2, pretrained_mod.model.config.num_attention_heads, seq_length, seq_length)
        if "hidden_states" in outputs:
            assert len(outputs["hidden_states"]) == pretrained_mod.model.config.num_hidden_layers + 1
            assert tuple(outputs["hidden_states"][0].shape) == (
                2, seq_length, pretrained_mod.model.config.hidden_size)
            assert np.allclose(np.array(pipeline_emb[0][0]),
                               outputs["hidden_states"][-1][0, :len(pipeline_emb[0][-1])].cpu().numpy(), rtol=0.0001)
        else:
            assert len(
                outputs["decoder_hidden_states"]) == pretrained_mod.model.config.num_hidden_layers + 1
            assert tuple(outputs["decoder_hidden_states"][0].shape) == (
                2, seq_length, pretrained_mod.model.config.hidden_size)
            assert np.allclose(np.array(pipeline_emb[0][0]),
                               outputs["decoder_hidden_states"][-1][0, :len(pipeline_emb[0][-1])].cpu().numpy(),
                               rtol=0.0001)


@pytest.mark.unittest
def test_feature_extraction_generate_outputs_pretokenized_single_sentence(raw_sentences_small, raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS_PRETOKENIZED:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(raw_sentences_small[0], pre_tokenized=True,
                                                  output_hidden_states=True)
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        pipeline_emb = extraction_pipeline(raw_small_source_string[0])
        if "hidden_states" in outputs:
            assert np.array_equal(np.array(pipeline_emb), outputs["hidden_states"][-1].cpu().numpy())
        else:
            assert np.array_equal(np.array(pipeline_emb), outputs["decoder_hidden_states"][-1].cpu().numpy())


@pytest.mark.unittest
def test_feature_extraction_generate_output_pretokenized_multi_sentence(raw_sentences_small, raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS_PRETOKENIZED:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(raw_sentences_small, pre_tokenized=True,
                                                  output_hidden_states=True)
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        pipeline_emb = extraction_pipeline(raw_small_source_string)
        if "hidden_states" in outputs:
            assert np.allclose(np.array(pipeline_emb[0][0]),
                               outputs["hidden_states"][-1][0, :len(pipeline_emb[0][-1])].cpu().numpy(), rtol=0.0001)
        else:
            assert np.allclose(np.array(pipeline_emb[0][0]),
                               outputs["decoder_hidden_states"][-1][0, :len(pipeline_emb[0][-1])].cpu().numpy(),
                               rtol=0.0001)


@pytest.mark.unittest
def test_feature_extraction_predict(raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        outputs = pretrained_mod.predict(raw_small_source_string)
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        pipeline_emb = extraction_pipeline(raw_small_source_string)
        assert np.array_equal(np.array(pipeline_emb[0]), np.array(outputs[0]))
        assert np.array_equal(np.array(pipeline_emb[1]), np.array(outputs[1]))


@pytest.mark.unittest
def test_feature_extraction_subword_merging_raw(raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        raw_outputs = np.array(extraction_pipeline(raw_small_source_string[0]))
        word_embeddings = pretrained_mod.generate_outputs(raw_small_source_string[0], pre_tokenized=False,
                                                          output_hidden_states=True, word_embeddings=True)
        if "hidden_states" in word_embeddings:
            word_embeddings = word_embeddings["hidden_states"][-1]
        else:
            word_embeddings = word_embeddings["decoder_hidden_states"][-1]
        for i in range(4):
            assert np.allclose(np.mean(raw_outputs[0][sub_word_indices_raw_small_string[model_name][i][0]:
                                                      sub_word_indices_raw_small_string[model_name][i][1]][:], 0),
                               word_embeddings[0][i], rtol=0.001)


@pytest.mark.unittest
def test_feature_extraction_subword_merging_pretokenized(raw_small_source_string):
    for model_name in TINY_MODEL_FECLASS_PRETOKENIZED:
        pretrained_mod = TransformerForFeatureExtraction(model_name, model_name)
        model = AutoModel.from_pretrained(model_name)
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        if tokenizer.pad_token is None:  # to allow batching during training, need to be able to pad
            tokenizer.pad_token = tokenizer.eos_token
            model.config.pad_token_id = model.config.eos_token_id
        extraction_pipeline = pipeline('feature-extraction', model=model, tokenizer=tokenizer)
        raw_outputs = np.array(extraction_pipeline(raw_small_source_string[0]))
        word_embeddings = pretrained_mod.generate_outputs(raw_small_source_string[0].split(), pre_tokenized=True,
                                                          output_hidden_states=True, word_embeddings=True)[
            "hidden_states"][-1]
        for i in range(4):
            assert np.allclose(np.mean(raw_outputs[0][sub_word_indices_raw_small_string[model_name][i][0]:
                                                      sub_word_indices_raw_small_string[model_name][i][1]][:], 0),
                               word_embeddings[0][i], rtol=0.001)


@pytest.mark.unittest
def test_seqclassifier_save_load(raw_small_source_string, tmpdir_factory):
    output_dir = tmpdir_factory.mktemp('model')
    save_dir = tmpdir_factory.mktemp('data')
    for model_name in TINY_MODEL_SEQCLASS:
        seq_classifier_model = TransformerForSequenceClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            seq_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        seq_classifier_model.train(train_text=raw_small_source_string, train_targets=[0, 1], output_dir=output_dir,
                                   num_train_epochs=1, report_to="none")
        seq_classifier_model.save(save_dir, save_tokenizer=False)
        outputs = seq_classifier_model.generate_outputs(raw_small_source_string)
        loaded_model = TransformerForSequenceClassification(str(save_dir), model_name, labels=["pos", "neg"])
        outputs2 = loaded_model.generate_outputs(raw_small_source_string)
        assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_seqclassifier_outputs(raw_small_source_string):
    for model_name in TINY_MODEL_SEQCLASS:
        pretrained_seqclassifier_model = TransformerForSequenceClassification(model_name, model_name,
                                                                              labels=["pos", "neg"])
        outputs = pretrained_seqclassifier_model.generate_outputs(raw_small_source_string, pre_tokenized=False,
                                                                  output_hidden_states=True,
                                                                  output_attentions=True)
        seq_length = SMALL_SOURCE_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["logits"].shape) == (2, 2)
        if "attentions" in outputs:
            assert len(outputs["attentions"]) == pretrained_seqclassifier_model.model.config.num_hidden_layers
            assert tuple(outputs["attentions"][0].shape) == (
                2, pretrained_seqclassifier_model.model.config.num_attention_heads, seq_length, seq_length)
        else:
            assert len(outputs["cross_attentions"]) == pretrained_seqclassifier_model.model.config.num_hidden_layers
            assert tuple(outputs["cross_attentions"][0].shape) == (
                2, pretrained_seqclassifier_model.model.config.num_attention_heads, seq_length, seq_length)
        if "hidden_states" in outputs:
            assert len(outputs["hidden_states"]) == pretrained_seqclassifier_model.model.config.num_hidden_layers + 1
            assert tuple(outputs["hidden_states"][0].shape) == (
                2, seq_length, pretrained_seqclassifier_model.model.config.hidden_size)
        else:
            assert len(
                outputs["decoder_hidden_states"]) == pretrained_seqclassifier_model.model.config.num_hidden_layers + 1
            assert tuple(outputs["decoder_hidden_states"][0].shape) == (
                2, seq_length, pretrained_seqclassifier_model.model.config.hidden_size)


@pytest.mark.unittest
def test_seqclassifier_predict():
    for model_name in TINY_MODEL_SEQCLASS:
        seq_classifier_model = TransformerForSequenceClassification(model_name, model_name, labels=["pos", "neg"])
        predictions = seq_classifier_model.predict("this is a test sentence")
        assert 0.0 <= predictions[0]["score"] <= 1.0


@pytest.mark.unittest
def test_seqclassifier_train(raw_small_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_SEQCLASS:
        seq_classifier_model = TransformerForSequenceClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            seq_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        seq_classifier_model.train(train_text=raw_small_source_string, train_targets=["pos", "neg"],
                                   output_dir=output_dir, num_train_epochs=1, report_to="none")
        assert not seq_classifier_model.model.training


@pytest.mark.unittest
def test_seqclassifier_train_freezebase(raw_small_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_SEQCLASS:
        seq_classifier_model = TransformerForSequenceClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            seq_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        seq_classifier_model.train(train_text=raw_small_source_string, train_targets=["pos", "neg"],
                                   output_dir=output_dir, num_train_epochs=1, freeze_base=True, report_to="none")
        assert not seq_classifier_model.model.training
        for param in seq_classifier_model.model.base_model.parameters():
            assert not param.requires_grad


@pytest.mark.unittest
def test_seqclassifier_train_multiplelearnrate(raw_small_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_SEQCLASS:
        seq_classifier_model = TransformerForSequenceClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            seq_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        seq_classifier_model.train(train_text=raw_small_source_string, train_targets=["pos", "neg"],
                                   output_dir=output_dir, num_train_epochs=1, task_learn_rate=0.001, report_to="none")
        assert not seq_classifier_model.model.training


@pytest.mark.unittest
def test_seqclassifier_train_with_eval(raw_small_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_SEQCLASS:
        seq_classifier_model = TransformerForSequenceClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            seq_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        seq_classifier_model.train(train_text=raw_small_source_string, train_targets=["pos", "neg"],
                                   eval_text=raw_small_source_string, eval_targets=["pos", "neg"],
                                   output_dir=output_dir, num_train_epochs=1, report_to="none")
        assert not seq_classifier_model.model.training


@pytest.mark.unittest
def test_tokenclassifier_save_load(raw_source_string, tmpdir_factory):
    output_dir = tmpdir_factory.mktemp('model')
    save_dir = tmpdir_factory.mktemp('data')
    for model_name in TINY_MODEL_TOKCLASS:
        token_classifier_model = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            token_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        token_classifier_model.train(train_text=[raw_source_string[0].split()], train_targets=[[1, 0, 1, 0, 1]],
                                     output_dir=output_dir, num_train_epochs=1, report_to="none")
        token_classifier_model.save(save_dir, save_tokenizer=False)
        outputs = token_classifier_model.generate_outputs(raw_source_string)
        loaded_model = TransformerForTokenClassification(str(save_dir), model_name, labels=["pos", "neg"])
        outputs2 = loaded_model.generate_outputs(raw_source_string)
        assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_tokenclassifier_outputs(raw_small_source_string):
    for model_name in TINY_MODEL_TOKCLASS:
        pretrained_mod = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        outputs = pretrained_mod.generate_outputs(raw_small_source_string, pre_tokenized=False,
                                                  output_hidden_states=True, output_attentions=True)
        seq_length = SMALL_SOURCE_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["logits"].shape) == (2, seq_length, 2)
        if "attentions" in outputs:
            assert len(outputs["attentions"]) == pretrained_mod.model.config.num_hidden_layers
            assert tuple(outputs["attentions"][0].shape) == (
                2, pretrained_mod.model.config.num_attention_heads, seq_length, seq_length)
        else:
            assert len(outputs["cross_attentions"]) == pretrained_mod.model.config.num_hidden_layers
            assert tuple(outputs["cross_attentions"][0].shape) == (
                2, pretrained_mod.model.config.num_attention_heads, seq_length, seq_length)
        if "hidden_states" in outputs:
            assert len(outputs["hidden_states"]) == pretrained_mod.model.config.num_hidden_layers + 1
            assert tuple(outputs["hidden_states"][0].shape) == (
                2, seq_length, pretrained_mod.model.config.hidden_size)
        else:
            assert len(
                outputs["decoder_hidden_states"]) == pretrained_mod.model.config.num_hidden_layers + 1
            assert tuple(outputs["decoder_hidden_states"][0].shape) == (
                2, seq_length, pretrained_mod.model.config.hidden_size)


@pytest.mark.unittest
def test_tokenclassifier_predict():
    for model_name in TINY_MODEL_TOKCLASS:
        token_classifier_model = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        predictions = token_classifier_model.predict("this is a test sentence")
        assert 0.0 <= predictions[0][0]["score"] <= 1.0
        assert predictions[0][0]["word"][0] == "t"
        assert predictions[0][0]["entity"] in ["pos", "neg"]


@pytest.mark.unittest
def test_tokenclassifier_train(raw_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_TOKCLASS:
        token_classifier_model = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            token_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        token_classifier_model.train(train_text=[raw_source_string[0].split()], train_targets=[[1, 0, 1, 0, 1]],
                                     output_dir=output_dir, num_train_epochs=1, report_to="none")
        assert not token_classifier_model.model.training


@pytest.mark.unittest
def test_tokenclassifier_train_freezebase(raw_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_TOKCLASS:
        token_classifier_model = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            token_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        token_classifier_model.train(train_text=[raw_source_string[0].split()], train_targets=[[1, 0, 1, 0, 1]],
                                     output_dir=output_dir, num_train_epochs=1, freeze_base=True, report_to="none")
        assert not token_classifier_model.model.training
        for param in token_classifier_model.model.base_model.parameters():
            assert not param.requires_grad


@pytest.mark.unittest
def test_tokenclassifier_train_multiplelearnrate(raw_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_TOKCLASS:
        token_classifier_model = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            token_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        token_classifier_model.train(train_text=[raw_source_string[0].split()], train_targets=[[1, 0, 1, 0, 1]],
                                     output_dir=output_dir, num_train_epochs=1, task_learn_rate=0.001, report_to="none")
        assert not token_classifier_model.model.training


@pytest.mark.unittest
def test_tokenclassifier_train_with_eval(raw_source_string, tmpdir_factory):
    for model_name in TINY_MODEL_TOKCLASS:
        token_classifier_model = TransformerForTokenClassification(model_name, model_name, labels=["pos", "neg"])
        if model_name in TINY_MODEL_MAXLENGTHS:
            token_classifier_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('data')
        token_classifier_model.train(train_text=[raw_source_string[0].split()], train_targets=[[1, 0, 1, 0, 1]],
                                     eval_text=[raw_source_string[0].split()], eval_targets=[[1, 0, 1, 0, 1]],
                                     output_dir=output_dir, num_train_epochs=1, report_to="none")
        assert not token_classifier_model.model.training


@pytest.mark.unittest
def test_questionanswer_outputs(question_answering_data):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(question=question_answering_data[0],
                                                  context=question_answering_data[1], pre_tokenized=False,
                                                  output_hidden_states=True, output_attentions=True)
        seq_length = QA_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["start_logits"].shape) == (2, seq_length)
        assert tuple(outputs["end_logits"].shape) == (2, seq_length)
        if "attentions" in outputs:
            assert len(outputs["attentions"]) == pretrained_mod.model.config.num_hidden_layers
            assert tuple(outputs["attentions"][0].shape) == (
                2, pretrained_mod.model.config.num_attention_heads, seq_length, seq_length)
        else:
            assert len(outputs["cross_attentions"]) == pretrained_mod.model.config.num_hidden_layers
            assert tuple(outputs["cross_attentions"][0].shape) == (
                2, pretrained_mod.model.config.num_attention_heads, seq_length, seq_length)
        if "hidden_states" in outputs:
            assert len(outputs["hidden_states"]) == pretrained_mod.model.config.num_hidden_layers + 1
            assert tuple(outputs["hidden_states"][0].shape) == (
                2, seq_length, pretrained_mod.model.config.hidden_size)
        else:
            assert len(
                outputs["decoder_hidden_states"]) == pretrained_mod.model.config.num_hidden_layers + 1
            assert tuple(outputs["decoder_hidden_states"][0].shape) == (
                2, seq_length, pretrained_mod.model.config.hidden_size)


@pytest.mark.unittest
def test_questionanswer_outputs_shared_context(question_answering_data):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(question=question_answering_data[0],
                                                  context=question_answering_data[1][0], pre_tokenized=False,
                                                  output_hidden_states=True, output_attentions=True)
        seq_length = QA_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["start_logits"].shape) == (2, seq_length)
        assert tuple(outputs["end_logits"].shape) == (2, seq_length)


@pytest.mark.unittest
def test_questionanswer_outputs_single_example(question_answering_data):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        outputs = pretrained_mod.generate_outputs(question=question_answering_data[0][0],
                                                  context=question_answering_data[1][0], pre_tokenized=False,
                                                  output_hidden_states=True, output_attentions=True)
        seq_length = QA_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["start_logits"].shape) == (1, seq_length)
        assert tuple(outputs["end_logits"].shape) == (1, seq_length)


@pytest.mark.unittest
def test_questionanswer_outputs_input_error(question_answering_data):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        with pytest.raises(RuntimeError) as excinfo:
            pretrained_mod.generate_outputs(question=question_answering_data[0][:1],
                                            context=question_answering_data[1],
                                            pre_tokenized=False, output_hidden_states=True, output_attentions=True)
        assert "don't have the same lengths" in str(excinfo.value)


@pytest.mark.unittest
def test_questionanswer_predict_strings(question_answering_data):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        print(pretrained_mod.tokenizer.__class__)
        print(pretrained_mod.tokenizer.model_input_names)
        if model_name in TINY_MODEL_MAXLENGTHS:
            pretrained_mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        predictions = pretrained_mod.predict(question=question_answering_data[0][0],
                                             context=question_answering_data[1][0])
        assert 0.0 <= predictions[0]["score"] <= 1.0
        assert predictions[0]["start"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])
        assert predictions[0]["end"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])


@pytest.mark.unittest
def test_questionanswer_predict_squadexample(question_answering_data):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            pretrained_mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        example = SquadExample(qas_id=0, question_text=question_answering_data[0][0],
                               context_text=question_answering_data[1][0], start_position_character=0,
                               title="", answer_text=question_answering_data[2][0])
        predictions = pretrained_mod.predict(example)
        assert 0.0 <= predictions[0]["score"] <= 1.0
        assert predictions[0]["start"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])
        assert predictions[0]["end"] < len(question_answering_data[0][0]) + len(question_answering_data[1][0])


@pytest.mark.unittest
def test_questionanswer_train(question_answering_data, tmpdir_factory):
    for model_name in TINY_MODEL_QACLASS:
        question_answer_model = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            question_answer_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        question_answer_model.train(train_question_texts=question_answering_data[0],
                                    train_context_texts=question_answering_data[1],
                                    train_answer_texts=question_answering_data[2],
                                    train_start_indices=question_answering_data[3],
                                    output_dir=output_dir, num_train_epochs=1, report_to="none",
                                    max_seq_length=question_answer_model.tokenizer.model_max_length,
                                    max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                    doc_stride=int(question_answer_model.tokenizer.model_max_length / 3)
                                    )
        assert not question_answer_model.model.training


@pytest.mark.unittest
def test_questionanswer_train_freezebase(question_answering_data, tmpdir_factory):
    for model_name in TINY_MODEL_QACLASS:
        question_answer_model = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            question_answer_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        question_answer_model.train(train_question_texts=question_answering_data[0],
                                    train_context_texts=question_answering_data[1],
                                    train_answer_texts=question_answering_data[2],
                                    train_start_indices=question_answering_data[3],
                                    output_dir=output_dir, num_train_epochs=1, freeze_base=True, report_to="none",
                                    max_seq_length=question_answer_model.tokenizer.model_max_length,
                                    max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                    doc_stride=int(question_answer_model.tokenizer.model_max_length / 3))
        assert not question_answer_model.model.training
        for param in question_answer_model.model.base_model.parameters():
            assert not param.requires_grad


@pytest.mark.unittest
def test_questionanswer_train_seq_len_error(question_answering_data, tmpdir_factory):
    for model_name in TINY_MODEL_QACLASS:
        pretrained_mod = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            pretrained_mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        with pytest.raises(ValueError) as excinfo:
            pretrained_mod.train(train_question_texts=question_answering_data[0],
                                 train_context_texts=question_answering_data[1],
                                 train_answer_texts=question_answering_data[2],
                                 train_start_indices=question_answering_data[3],
                                 output_dir=output_dir, num_train_epochs=1, freeze_base=True, report_to="none",
                                 max_seq_length=pretrained_mod.tokenizer.model_max_length + 12)
        assert "max_seq_length argument is greater than" in str(excinfo.value)


@pytest.mark.unittest
def test_questionanswer_train_multiplelearnrate(question_answering_data, tmpdir_factory):
    for model_name in TINY_MODEL_QACLASS:
        question_answer_model = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            question_answer_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        question_answer_model.train(train_question_texts=question_answering_data[0],
                                    train_context_texts=question_answering_data[1],
                                    train_answer_texts=question_answering_data[2],
                                    train_start_indices=question_answering_data[3],
                                    output_dir=output_dir, num_train_epochs=1, freeze_base=True, report_to="none",
                                    max_seq_length=question_answer_model.tokenizer.model_max_length,
                                    max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                    doc_stride=int(question_answer_model.tokenizer.model_max_length / 3),
                                    task_learn_rate=0.001, )
        assert not question_answer_model.model.training


@pytest.mark.unittest
def test_questionanswer_train_with_eval(question_answering_data, tmpdir_factory):
    for model_name in TINY_MODEL_QACLASS:
        question_answer_model = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            question_answer_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        question_answer_model.train(train_question_texts=question_answering_data[0],
                                    train_context_texts=question_answering_data[1],
                                    train_answer_texts=question_answering_data[2],
                                    train_start_indices=question_answering_data[3],
                                    eval_question_texts=question_answering_data[0],
                                    eval_context_texts=question_answering_data[1],
                                    eval_answer_texts=question_answering_data[2],
                                    eval_start_indices=question_answering_data[3],
                                    output_dir=output_dir, num_train_epochs=1, report_to="none",
                                    max_seq_length=question_answer_model.tokenizer.model_max_length,
                                    max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                    doc_stride=int(question_answer_model.tokenizer.model_max_length / 3)
                                    )
        assert not question_answer_model.model.training


@pytest.mark.unittest
def test_questionanswer_save_load(question_answering_data, tmpdir_factory):
    for model_name in TINY_MODEL_QACLASS:
        question_answer_model = TransformerForQuestionAnswering(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            question_answer_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        question_answer_model.train(train_question_texts=question_answering_data[0],
                                    train_context_texts=question_answering_data[1],
                                    train_answer_texts=question_answering_data[2],
                                    train_start_indices=question_answering_data[3],
                                    output_dir=output_dir, num_train_epochs=1, report_to="none",
                                    max_seq_length=question_answer_model.tokenizer.model_max_length,
                                    max_query_length=int(question_answer_model.tokenizer.model_max_length / 3),
                                    doc_stride=int(question_answer_model.tokenizer.model_max_length / 3)
                                    )
        save_dir = tmpdir_factory.mktemp('data')
        question_answer_model.save(save_dir, save_tokenizer=False)
        outputs = question_answer_model.generate_outputs(question=question_answering_data[0],
                                                         context=question_answering_data[1],
                                                         pre_tokenized=False, output_hidden_states=False,
                                                         output_attentions=False)
        loaded_model = TransformerForQuestionAnswering(str(save_dir), model_name)
        outputs2 = loaded_model.generate_outputs(question=question_answering_data[0],
                                                 context=question_answering_data[1],
                                                 pre_tokenized=False, output_hidden_states=False,
                                                 output_attentions=False)
        assert np.array_equal(outputs["start_logits"].numpy(), outputs2["start_logits"].numpy(), )
        assert np.array_equal(outputs["end_logits"].numpy(), outputs2["end_logits"].numpy(), )


@pytest.mark.unittest
def test_multiplechoice_outputs_input_error(multiple_choice_example):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]

        # test using only one question/context
        with pytest.raises(RuntimeError) as excinfo:
            multiple_choice_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                                   choices=[multiple_choice_example[1]],
                                                   pre_tokenized=False, output_hidden_states=True,
                                                   output_attentions=True)
        assert "Number of questions does not match number of sets of choices" in str(excinfo.value)


@pytest.mark.unittest
def test_multiplechoice_predict(multiple_choice_example):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        predictions = multiple_choice_model.predict(questions=multiple_choice_example[0],
                                                    choices=multiple_choice_example[1])
        assert 0.0 <= predictions[0]["score"] <= 1.0


@pytest.mark.unittest
def test_multiplechoice_outputs(multiple_choice_example):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        outputs = multiple_choice_model.generate_outputs(questions=multiple_choice_example[0],
                                                         choices=multiple_choice_example[1],
                                                         pre_tokenized=False, output_hidden_states=True,
                                                         output_attentions=True)
        seq_length = MC_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["logits"].shape) == (1, 2)
        assert len(outputs["attentions"]) == multiple_choice_model.model.config.num_hidden_layers
        assert tuple(outputs["attentions"][0].shape) == (1, 2, multiple_choice_model.model.config.num_attention_heads,
                                                         seq_length, seq_length)
        assert len(outputs["hidden_states"]) == multiple_choice_model.model.config.num_hidden_layers + 1
        assert tuple(outputs["hidden_states"][0].shape) == (1, 2, seq_length,
                                                            multiple_choice_model.model.config.hidden_size)
        assert tuple(outputs["offset_mappings"].shape) == (1, 2, seq_length, 2)


@pytest.mark.unittest
def test_multiplechoice_outputs_batch(multiple_choice_example):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        outputs = multiple_choice_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                                         choices=[multiple_choice_example[1]] * 2,
                                                         pre_tokenized=False, output_hidden_states=True,
                                                         output_attentions=True)
        seq_length = MC_STRING_SUBTOKENS[model_name]
        assert tuple(outputs["logits"].shape) == (2, 2)
        assert len(outputs["attentions"]) == multiple_choice_model.model.config.num_hidden_layers
        assert tuple(outputs["attentions"][0].shape) == (2, 2, multiple_choice_model.model.config.num_attention_heads,
                                                         seq_length, seq_length)
        assert len(outputs["hidden_states"]) == multiple_choice_model.model.config.num_hidden_layers + 1
        assert tuple(outputs["hidden_states"][0].shape) == (2, 2, seq_length,
                                                            multiple_choice_model.model.config.hidden_size)
        assert tuple(outputs["offset_mappings"].shape) == (2, 2, seq_length, 2)


@pytest.mark.unittest
def test_multiplechoice_train(multiple_choice_example, tmpdir_factory):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                    train_choices_texts=[multiple_choice_example[1]] * 5,
                                    train_labels=[0] * 5,
                                    per_device_train_batch_size=2,
                                    output_dir=output_dir, num_train_epochs=1, report_to="none")
        assert not multiple_choice_model.model.training


@pytest.mark.unittest
def test_multiplechoice_train_freezebase(multiple_choice_example, tmpdir_factory):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                    train_choices_texts=[multiple_choice_example[1]] * 5,
                                    train_labels=[0] * 5,
                                    per_device_train_batch_size=2,
                                    output_dir=output_dir, num_train_epochs=1, freeze_base=True, report_to="none")
        assert not multiple_choice_model.model.training
        for param in multiple_choice_model.model.base_model.parameters():
            assert not param.requires_grad


@pytest.mark.unittest
def test_multiplechoice_train_with_validation(multiple_choice_example, tmpdir_factory):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                    eval_question_texts=[multiple_choice_example[0]] * 5,
                                    train_choices_texts=[multiple_choice_example[1]] * 5,
                                    eval_choices_texts=[multiple_choice_example[1]] * 5,
                                    train_labels=[0] * 5,
                                    eval_labels=[0] * 5,
                                    per_device_train_batch_size=2,
                                    output_dir=output_dir, num_train_epochs=1, report_to="none")
        assert not multiple_choice_model.model.training


@pytest.mark.unittest
def test_multiplechoice_train_multiple_learnrate(multiple_choice_example, tmpdir_factory):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                    train_choices_texts=[multiple_choice_example[1]] * 5,
                                    train_labels=[0] * 5,
                                    per_device_train_batch_size=2,
                                    output_dir=output_dir, num_train_epochs=1, task_learn_rate=0.001, report_to="none")
        assert not multiple_choice_model.model.training


@pytest.mark.unittest
def test_multiplechoice_train_dir_error(multiple_choice_example):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        # test using only one question/context
        with pytest.raises(RuntimeError) as excinfo:
            multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 2,
                                        train_choices_texts=[multiple_choice_example[1]], report_to="none")
        assert "output directory" in str(excinfo.value)


@pytest.mark.unittest
def test_multiplechoice_train_label_error(multiple_choice_example, tmpdir_factory):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        output_dir = tmpdir_factory.mktemp('model')
        # test using only one question/context
        with pytest.raises(RuntimeError) as excinfo:
            multiple_choice_model.train(output_dir, train_question_texts=[multiple_choice_example[0]] * 2,
                                        train_choices_texts=[multiple_choice_example[1]] * 2, report_to="none")
        assert "Incomplete training data" in str(excinfo.value)


@pytest.mark.unittest
def test_multiplechoice_save_load(multiple_choice_example, tmpdir_factory):
    for model_name in TINY_MODEL_MCCLASS:
        multiple_choice_model = TransformerForMultipleChoice(model_name, model_name)
        if model_name in TINY_MODEL_MAXLENGTHS:
            multiple_choice_model.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        output_dir = tmpdir_factory.mktemp('model')
        multiple_choice_model.train(train_question_texts=[multiple_choice_example[0]] * 5,
                                    train_choices_texts=[multiple_choice_example[1]] * 5,
                                    train_labels=[0] * 5,
                                    per_device_train_batch_size=2,
                                    output_dir=output_dir, num_train_epochs=1, report_to="none")
        save_dir = tmpdir_factory.mktemp('data')
        multiple_choice_model.save(save_dir, save_tokenizer=False)
        outputs = multiple_choice_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                                         choices=[multiple_choice_example[1]] * 2,
                                                         pre_tokenized=False, output_hidden_states=False,
                                                         output_attentions=False)
        loaded_model = TransformerForMultipleChoice(str(save_dir), model_name)
        outputs2 = loaded_model.generate_outputs(questions=[multiple_choice_example[0]] * 2,
                                                 choices=[multiple_choice_example[1]] * 2,
                                                 pre_tokenized=False, output_hidden_states=False,
                                                 output_attentions=False)
        assert np.array_equal(outputs["logits"].numpy(), outputs2["logits"].numpy(), )


@pytest.mark.unittest
def test_coref_outputs_metadata_error(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, _, _ = raw_coref_data
        with pytest.raises(RuntimeError) as excinfo:
            mod.generate_outputs(coref_documents[0], pre_tokenized=True)
        assert "use metadata" in str(excinfo.value)


@pytest.mark.unittest
def test_coref_outputs_metadata_genre_mapping_error(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        with pytest.raises(RuntimeError) as excinfo:
            mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre="unknown")
        assert "genre id mapping" in str(excinfo.value)


@pytest.mark.unittest
def test_coref_outputs_metadata_genre_type_error(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        with pytest.raises(RuntimeError) as excinfo:
            mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre=[1])
        assert "string or int" in str(excinfo.value)


@pytest.mark.unittest
def test_coref_outputs_metadata_speakerid_error(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        with pytest.raises(RuntimeError) as excinfo:
            mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0][:2], genre=1)
        assert "Lengths of speaker ids and text arguments are different" in str(excinfo.value)


@pytest.mark.unittest
def test_coref_outputs_max_num_segments(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        with pytest.warns(RuntimeWarning):
            mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre=1,
                                 max_segment_len=12, max_segments=1)


@pytest.mark.unittest
def test_coref_outputs_pretokenized(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        results = mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre=1)
        assert set(list(results.keys())) == {"candidate_starts", "candidate_ends", "candidate_mention_scores",
                                             "top_span_starts", "top_span_ends", "top_antecedents",
                                             "top_antecedent_scores",
                                             "flattened_ids", "flattened_text", "loss"}


@pytest.mark.unittest
def test_coref_outputs_raw(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        input_docs = [' '.join(sent) for sent in coref_documents[0]]
        input_speakers = [sent[0] for sent in coref_speakers[0]]
        results = mod.generate_outputs(input_docs, pre_tokenized=False, speaker_ids=input_speakers, genre=1)
        assert set(list(results.keys())) == {"candidate_starts", "candidate_ends", "candidate_mention_scores",
                                             "top_span_starts", "top_span_ends", "top_antecedents",
                                             "top_antecedent_scores",
                                             "flattened_ids", "flattened_text", "loss"}


@pytest.mark.unittest
def test_coref_outputs_single_sent(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        results = mod.generate_outputs(coref_documents[0][0], pre_tokenized=True, speaker_ids=coref_speakers[0][0],
                                       genre=1)
        assert set(list(results.keys())) == {"candidate_starts", "candidate_ends", "candidate_mention_scores",
                                             "top_span_starts", "top_span_ends", "top_antecedents",
                                             "top_antecedent_scores",
                                             "flattened_ids", "flattened_text", "loss"}


@pytest.mark.unittest
def test_coref_predict(raw_coref_data):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        coref_documents, _, coref_speakers, _ = raw_coref_data
        results = mod.predict(coref_documents[0][0], pre_tokenized=True, speaker_ids=coref_speakers[0][0], genre=1)
        for cluster_dict in results:
            assert len(cluster_dict["cluster_ids"]) == len(cluster_dict["cluster_tokens"])


@pytest.mark.unittest
def test_coref_train_single_learnrate(raw_coref_data, tmpdir_factory):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        mod_path = tmpdir_factory.mktemp('mod_path')
        coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
        max_segment_len = 15
        mod.train(mod_path, max_segment_len=max_segment_len, max_segments=5, task_learn_rate=None,
                  train_documents=coref_documents, train_cluster_ids=coref_cluster_ids,
                  train_speaker_ids=coref_speakers,
                  train_genres=coref_genres, num_train_epochs=1, report_to="none")
        assert not mod.model.training


@pytest.mark.unittest
def test_coref_train_single_learnrate_freezebase(raw_coref_data, tmpdir_factory):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        mod_path = tmpdir_factory.mktemp('mod_path')
        coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
        max_segment_len = 15
        mod.train(mod_path, max_segment_len=max_segment_len, max_segments=5, task_learn_rate=None,
                  train_documents=coref_documents, train_cluster_ids=coref_cluster_ids,
                  train_speaker_ids=coref_speakers,
                  train_genres=coref_genres, num_train_epochs=1, freeze_base=True, report_to="none")
        assert not mod.model.training
        for param in mod.model.base_model.parameters():
            assert not param.requires_grad


@pytest.mark.unittest
def test_coref_train_single_learnrate_eval(raw_coref_data, tmpdir_factory):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        mod_path = tmpdir_factory.mktemp('mod_path')
        coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
        max_segment_len = 15
        mod.train(mod_path, max_segment_len=max_segment_len, max_segments=5, task_learn_rate=None,
                  train_documents=coref_documents, train_cluster_ids=coref_cluster_ids,
                  train_speaker_ids=coref_speakers,
                  train_genres=coref_genres, eval_documents=coref_documents, eval_cluster_ids=coref_cluster_ids,
                  eval_speaker_ids=coref_speakers, eval_genres=coref_genres, num_train_epochs=1, report_to="none")
        assert not mod.model.training


@pytest.mark.unittest
def test_coref_train_multiple_learnrate_custom_trainer(raw_coref_data, tmpdir_factory):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        mod_path = tmpdir_factory.mktemp('mod_path')
        coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
        max_segment_len = 15
        base_learn_rate = 0.001
        task_learn_rate = 0.01
        task_weight_decay = 0.01
        max_segments = 5
        train_dataset = MangoesCoreferenceDataset(mod.tokenizer, mod.model.config.task_specific_params["use_metadata"],
                                                  max_segment_len, max_segments, coref_documents, coref_cluster_ids,
                                                  coref_speakers, coref_genres)
        train_args = TrainingArguments(output_dir=mod_path, per_device_eval_batch_size=1,
                                       per_device_train_batch_size=1, num_train_epochs=1, learning_rate=base_learn_rate,
                                       report_to="none")
        trainer = CoreferenceFineTuneTrainer(task_learn_rate, task_weight_decay, mod.model.base_model_prefix, mod.model,
                                             args=train_args, train_dataset=train_dataset, tokenizer=mod.tokenizer)

        mod.train(mod_path, trainer=trainer)
        assert trainer.optimizer.param_groups[0]["initial_lr"] == base_learn_rate
        assert trainer.optimizer.param_groups[2]["initial_lr"] == task_learn_rate
        assert trainer.optimizer.param_groups[0]["weight_decay"] == train_args.weight_decay
        assert trainer.optimizer.param_groups[2]["weight_decay"] == task_weight_decay


@pytest.mark.unittest
def test_coref_load_full(tmpdir_factory):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        mod_path = tmpdir_factory.mktemp('mod_path')
        mod.save(mod_path)
        mod = TransformerForCoreferenceResolution(str(mod_path), model_name)
        assert mod.model.config.task_specific_params["max_span_width"] == 1
        assert mod.model.config.task_specific_params["ffnn_hidden_size"] == 100
        assert mod.model.config.task_specific_params["top_span_ratio"] == 1.0
        assert mod.model.config.task_specific_params["use_metadata"]
        assert mod.model.config.task_specific_params["max_top_antecendents"] == 50
        assert mod.model.config.task_specific_params["metadata_feature_size"] == 20


@pytest.mark.unittest
def test_coref_train_save_load(raw_coref_data, tmpdir_factory):
    for model_name in TINY_MODEL_COREFCLASS:
        mod = TransformerForCoreferenceResolution(model_name, model_name, max_span_width=1, top_span_ratio=1.0,
                                                  ffnn_hidden_size=100,
                                                  use_metadata=True, hidden_size=120, num_hidden_layers=2,
                                                  intermediate_size=256)
        if model_name in TINY_MODEL_MAXLENGTHS:
            mod.tokenizer.model_max_length = TINY_MODEL_MAXLENGTHS[model_name]
        mod_path = tmpdir_factory.mktemp('mod_path')
        coref_documents, coref_cluster_ids, coref_speakers, coref_genres = raw_coref_data
        max_segment_len = 15
        mod.train(mod_path, max_segment_len=max_segment_len, max_segments=5, task_learn_rate=None,
                  train_documents=coref_documents, train_cluster_ids=coref_cluster_ids,
                  train_speaker_ids=coref_speakers,
                  train_genres=coref_genres, eval_documents=coref_documents, eval_cluster_ids=coref_cluster_ids,
                  eval_speaker_ids=coref_speakers, eval_genres=coref_genres, num_train_epochs=1, report_to="none")
        save_dir = tmpdir_factory.mktemp('data')
        mod.save(save_dir, save_tokenizer=False)
        results = mod.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0], genre=1)
        loaded_model = TransformerForCoreferenceResolution(str(save_dir), model_name, pretrained=True)
        outputs2 = loaded_model.generate_outputs(coref_documents[0], pre_tokenized=True, speaker_ids=coref_speakers[0],
                                                 genre=1)
        assert np.array_equal(mod.model.features_model.embeddings.word_embeddings.weight.detach().numpy(),
                              loaded_model.model.features_model.embeddings.word_embeddings.weight.detach().numpy())
        assert np.array_equal(mod.model.mention_scorer[0].weight.detach().numpy(),
                              loaded_model.model.mention_scorer[0].weight.detach().numpy())
        assert np.array_equal(results["candidate_starts"].numpy(), outputs2["candidate_starts"].numpy(), )
        assert np.array_equal(results["candidate_ends"].numpy(), outputs2["candidate_ends"].numpy(), )
        assert np.array_equal(results["candidate_mention_scores"].numpy(),
                              outputs2["candidate_mention_scores"].numpy(), )
        assert np.array_equal(results["top_span_starts"].numpy(), outputs2["top_span_starts"].numpy(), )
        assert np.array_equal(results["top_span_ends"].numpy(), outputs2["top_span_ends"].numpy(), )
        assert np.array_equal(results["top_antecedent_scores"].numpy(), outputs2["top_antecedent_scores"].numpy(), )
        assert np.array_equal(results["top_antecedents"].numpy(), outputs2["top_antecedents"].numpy(), )
