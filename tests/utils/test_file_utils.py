# -*- coding: utf-8 -*-
import pytest
import os

from mangoes.utils.file_utils import download_and_cache


# ###########################################################################################
# ### Unit tests
@pytest.mark.unittest
def test_file_download():
    url = "https://dumps.wikimedia.org/wikidatawiki/latest/wikidatawiki-latest-abstract.xml.gz-rss.xml"
    output_path = download_and_cache(url, "test_file_download", force_download=True)
    assert os.path.exists(output_path)
