import argparse
import evaluate
import datetime
from datasets import load_dataset
from mangoes.modeling import *
from transformers import AutoTokenizer
import os
import json

metric = evaluate.load("glue", "cola")


def compute_metrics(p):
    preds = p.predictions[0] if isinstance(p.predictions, tuple) else p.predictions
    preds = np.argmax(preds, axis=1)
    result = metric.compute(predictions=preds, references=p.label_ids)
    if len(result) > 1:
        result["combined_score"] = np.mean(list(result.values())).item()
    return result


def load_model(model_name, label2id):
    if model_name in ["ernie", "kadapter", "libert", "kepler", "ebert", "hck"]:
        if model_name == "ernie":
            model = ErnieForSequenceClassification.from_pretrained(None, label2id=label2id)
            tokenizer = ErnieTokenizer.from_pretrained(None)
        elif model_name == "kadapter":
            model = KAdapterForSequenceClassification.from_pretrained(None, label2id=label2id)
            tokenizer = AutoTokenizer.from_pretrained(KADAPTER_PRETRAINED_TOKENIZER_NAME)
        elif model_name == "libert":
            model = LibertForSequenceClassification.from_pretrained(None, label2id=label2id)
            tokenizer = LibertTokenizerFast.from_pretrained(None)
        elif model_name == "kepler":
            model = KeplerForSequenceClassification.from_pretrained(None, label2id=label2id)
            tokenizer = AutoTokenizer.from_pretrained(KEPLER_PRETRAINED_TOKENIZER_NAME)
        elif model_name == "hck":
            model = HCKForSequenceClassification.from_pretrained(None, label2id=label2id)
            tokenizer = HCKTokenizerFast.from_pretrained(None)
        mangoes_model = TransformerForSequenceClassification(model, tokenizer, label2id=label2id)
    else:
        mangoes_model = TransformerForSequenceClassification(model_name, model_name, label2id=label2id)
    return mangoes_model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', type=str, required=True)
    args = parser.parse_args()
    print(f"Model: {args.model_name}")

    label2id = {v: i for i, v in enumerate(["unacceptable", "acceptable"])}
    id2label = {id: label for label, id in label2id.items()}

    mangoes_model = load_model(args.model_name, label2id)

    dataset = load_dataset("glue", "cola")
    train_dataset = MangoesTextClassificationDataset(list(dataset["train"]["sentence"]),
                                                     list(dataset["train"]["label"]), mangoes_model.tokenizer,
                                                     max_len=512)
    valid_dataset = MangoesTextClassificationDataset(list(dataset["validation"]["sentence"]),
                                                     list(dataset["validation"]["label"]), mangoes_model.tokenizer,
                                                     max_len=512)
    test_dataset = MangoesTextClassificationDataset(list(dataset["test"]["sentence"]),
                                                    list(dataset["test"]["label"]), mangoes_model.tokenizer,
                                                    max_len=512, include_labels=False)
    learn_rates = [5e-5, 3e-5, 2e-5]
    batch_sizes = [16, 32]
    num_epochs = [2, 3, 4]
    output_dir = f"./model_training_{str(datetime.datetime.now()).replace(' ', '')}"
    best_valid_score = None
    best_hyperparams = None
    test_pred = None
    use_gradient_acc = False
    for lr in learn_rates:
        for bs in batch_sizes:
            for n_epoch in num_epochs:
                mangoes_model = load_model(args.model_name, label2id)
                mangoes_model.train(train_dataset=train_dataset, eval_dataset=valid_dataset,
                                    output_dir=output_dir, overwrite_output_dir=True,
                                    max_len=512, num_train_epochs=n_epoch,
                                    per_device_train_batch_size=4 if use_gradient_acc else bs,
                                    per_device_eval_batch_size=4 if use_gradient_acc else bs,
                                    logging_strategy="no", learning_rate=lr, evaluation_strategy="no",
                                    compute_metrics=compute_metrics, save_strategy="no",
                                    gradient_accumulation_steps=int(bs / 4) if use_gradient_acc else 1,
                                    eval_accumulation_steps=16 if use_gradient_acc else None)
                valid_score = mangoes_model.trainer.evaluate(valid_dataset)
                print(f"valid score: {valid_score}")
                if best_valid_score is None or best_valid_score < valid_score["eval_matthews_correlation"]:
                    test_pred = mangoes_model.trainer.predict(test_dataset)
                    best_valid_score = valid_score["eval_matthews_correlation"]
                    best_hyperparams = (lr, bs, n_epoch)

    print(f"Model: {args.model_name}")
    print(f"Best valid score: {best_valid_score}")
    print(f"Best hyperparams: {best_hyperparams}")
    results = {"model_name": args.model_name, "valid_score": best_valid_score, "hyperparams": best_hyperparams,
               "task": "cola"}
    output_dir = f"outputs/{args.model_name}"
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    with open(os.path.join(output_dir, "cola_results.json"), "w") as f:
        json.dump(results, f)
    preds = test_pred.predictions[0] if isinstance(test_pred.predictions, tuple) else test_pred.predictions
    preds = np.argmax(preds, axis=1)
    indices = list(dataset["test"]["idx"])
    with open(os.path.join(output_dir, "CoLA.tsv"), "w") as f:
        f.write("index\tprediction\n")
        for i in range(preds.shape[0]):
            f.write(f"{indices[i]}\t{preds[i]}" + ("\n" if i < preds.shape[0] - 1 else ""))
